package com.lsw.cvsinventorymanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CvsInventoryManagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(CvsInventoryManagerApplication.class, args);
    }

}
