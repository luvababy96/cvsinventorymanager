package com.lsw.cvsinventorymanager.controller;

import com.lsw.cvsinventorymanager.model.InventoryExpirationDateChangeRequest;
import com.lsw.cvsinventorymanager.model.InventoryItem;
import com.lsw.cvsinventorymanager.model.InventoryRequest;
import com.lsw.cvsinventorymanager.service.InventoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/inventory")
public class InventoryController {

    private final InventoryService inventoryService;

    @PostMapping("/data")
    public String setInventory(@RequestBody @Valid InventoryRequest request) {
      inventoryService.setInventory(request);

      return "OK";
    }

    @GetMapping("/all")
    public List<InventoryItem> getInventories(){
        List<InventoryItem> result = inventoryService.getInventoryList();

        return result;

    }

    @PutMapping("/change-date/id/{id}")
    public String putExpirationDate(@PathVariable long id, @RequestBody @Valid InventoryExpirationDateChangeRequest request) {
        inventoryService.putExpirationDate(id,request);

        return "OK";
    }

    @DeleteMapping("/delete-inventory/id/{id}")
    public String delInventory (@PathVariable long id) {
        inventoryService.delInventory(id);

        return "OK";
    }
}
