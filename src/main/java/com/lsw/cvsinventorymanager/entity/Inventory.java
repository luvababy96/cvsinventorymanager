package com.lsw.cvsinventorymanager.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@Setter
public class Inventory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false,length = 43)
    private String barCode;

    @Column(nullable = false,length = 20)
    private String category;

    @Column(nullable = false,length = 30)
    private String productName;

    @Column(nullable = false,length = 10)
    private String productPlace;

    @Column(nullable = false,length = 30)
    private String clientName;

    @Column(nullable = false)
    private Integer purchasePrice;

    @Column(nullable = false)
    private Integer salesPrice;

    @Column(nullable = false)
    private Integer inventoryQuantity;

    @Column(nullable = false)
    private LocalDate receivingDate;

    @Column(nullable = false)
    private LocalDate expirationDate;


}
