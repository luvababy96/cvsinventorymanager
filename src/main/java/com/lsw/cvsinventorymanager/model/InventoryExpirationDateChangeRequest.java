package com.lsw.cvsinventorymanager.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class InventoryExpirationDateChangeRequest {

    @NotNull
    private LocalDate expirationDate;

}
