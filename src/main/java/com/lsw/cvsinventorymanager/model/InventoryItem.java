package com.lsw.cvsinventorymanager.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class InventoryItem {

    private Long id;

    private String category;

    private String productName;

    private String productPlace;

    private Integer purchasePrice;

    private Integer salesPrice;

    private String margin;

    private Integer inventoryQuantity;

    private LocalDate receivingDate;

    private String statusDate;





}
