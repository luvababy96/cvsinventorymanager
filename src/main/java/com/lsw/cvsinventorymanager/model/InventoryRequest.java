package com.lsw.cvsinventorymanager.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class InventoryRequest {

    @NotNull
    @Length(min = 6,max = 43)
    private String barCode;

    @NotNull
    @Length(min = 1,max = 20)
    private String category;

    @NotNull
    @Length(min = 1,max = 30)
    private String productName;

    @NotNull
    @Length(min = 1,max = 10)
    private String productPlace;

    @NotNull
    @Length(min = 1,max = 30)
    private String clientName;

    @NotNull
    private Integer purchasePrice;

    @NotNull
    private Integer salesPrice;

    @NotNull
    private Integer inventoryQuantity;

    @NotNull
    private LocalDate expirationDate;
}
