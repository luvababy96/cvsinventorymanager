package com.lsw.cvsinventorymanager.repository;

import com.lsw.cvsinventorymanager.entity.Inventory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InventoryRepository extends JpaRepository<Inventory, Long> {
}
