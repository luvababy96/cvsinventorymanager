package com.lsw.cvsinventorymanager.service;

import com.lsw.cvsinventorymanager.entity.Inventory;
import com.lsw.cvsinventorymanager.model.InventoryExpirationDateChangeRequest;
import com.lsw.cvsinventorymanager.model.InventoryItem;
import com.lsw.cvsinventorymanager.model.InventoryRequest;
import com.lsw.cvsinventorymanager.repository.InventoryRepository;
import lombok.RequiredArgsConstructor;
import org.hibernate.cache.spi.support.AbstractReadWriteAccess;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class InventoryService {

    private final InventoryRepository inventoryRepository;

    public void setInventory(InventoryRequest request) {
        Inventory addDate = new Inventory();
        addDate.setBarCode(request.getBarCode());
        addDate.setCategory(request.getCategory());
        addDate.setProductName(request.getProductName());
        addDate.setProductPlace(request.getProductPlace());
        addDate.setClientName(request.getClientName());
        addDate.setPurchasePrice(request.getPurchasePrice());
        addDate.setSalesPrice(request.getSalesPrice());
        addDate.setInventoryQuantity(request.getInventoryQuantity());
        addDate.setReceivingDate(LocalDate.now());
        addDate.setExpirationDate(request.getExpirationDate());

        inventoryRepository.save(addDate);

    }

    public List<InventoryItem> getInventoryList() {
        List<Inventory> originList = inventoryRepository.findAll();

        List<InventoryItem> result = new LinkedList<>();

        for(Inventory item : originList) {
            InventoryItem addItem = new InventoryItem();
            addItem.setId(item.getId());
            addItem.setCategory(item.getCategory());
            addItem.setProductName(item.getProductName());
            addItem.setProductPlace(item.getProductPlace());
            addItem.setPurchasePrice(item.getPurchasePrice());
            addItem.setSalesPrice(item.getSalesPrice());

            int marginResult = item.getSalesPrice() - item.getPurchasePrice();
            double marginPercent = ((double) marginResult / (double) item.getSalesPrice()) * 100;
            addItem.setMargin(marginResult+ "/" + marginPercent + "%");

            addItem.setInventoryQuantity(item.getInventoryQuantity());
            addItem.setReceivingDate(item.getReceivingDate());

            String statusName = "정상";
            if(item.getExpirationDate().isBefore(LocalDate.now())){
                statusName = "폐기";
            } else if (item.getExpirationDate().minusDays(1).equals(LocalDate.now())) {
                statusName = "임박";
            }

            addItem.setStatusDate(item.getExpirationDate() + "(" + statusName + ")");

            result.add(addItem);


        }

        return result;
    }

    public void putExpirationDate(long id, InventoryExpirationDateChangeRequest request) {
        Inventory originData = inventoryRepository.findById(id).orElseThrow();
        originData.setExpirationDate(request.getExpirationDate());

        inventoryRepository.save(originData);
    }

    public void delInventory(long id) {
        inventoryRepository.deleteById(id);
    }




}
